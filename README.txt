Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
Produced at the Lawrence Livermore National Laboratory. 
Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
LLNL-CODE-624712. 
All rights reserved.

This file is part of LRIOT, Version 1.0. 
For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html

--------------------------------------------------------------------------------

Thank you for downloading the Livermore Random I/O Testbench (LRIOT) repository.
This code was developed at Lawrence Livermore National Laboratory to
provide a tool for testing high-performance storage, especially NVRAM, and for
generating I/O patterns that mimic applications of interest.

Usage: (See README_usage.txt for further details)
 
Command-line only
    mpirun -np $N LRIOT <args>

Single behavior + command-line overrides
    mpirun -np $N LRIOT <behavior file> <behavior name 1> [args]

Multiple behaviors
    mpirun -np $N LRIOT <behavior file> <behavior name 1 | [behavior name n]>

Behaviors can specify

# threads
target devices / files
# cores / core binding patters
sequence
I/O method

Publications that use LRIOT:

A paper utilizing LRIOT was published at the DISCS 2012 workshop.
The paper "DI-MMAP: A High Performance Memory-Map Runtime for
Data-Intensive Application" and its associated slides can be found at:
http://discl.cs.ttu.edu/discs/docs/discs2012_submission_13_1.pdf
http://discl.cs.ttu.edu/discs/docs/DI-MMAP_A_High_Performance_Memory_Map_Runtime_1.pdf
