/*
 * Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-624712. 
 * All rights reserved.
 * 
 * This file is part of LRIOT, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Additional BSD Notice. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * • Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the disclaimer below.
 * 
 * • Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the disclaimer (as noted below) in the
 *   documentation and/or other materials provided with the distribution.
 * 
 * • Neither the name of the LLNS/LLNL nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY, LLC,
 * THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * Additional BSD Notice
 * 
 * 1. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at Lawrence Livermore
 * National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * 2. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or
 * process disclosed, or represents that its use would not infringe
 * privately-owned rights.
 * 
 * 3. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring
 * by the United States Government or Lawrence Livermore National Security,
 * LLC. The views and opinions of authors expressed herein do not necessarily
 * state or reflect those of the United States Government or Lawrence Livermore
 * National Security, LLC, and shall not be used for advertising or product
 * endorsement purposes.
 * 
 */
#ifndef PROFILE
#define PROFILE

#include "weighted_random.hpp"

#include <iostream>
#include <string>
#include <vector>

class profile
{
    public:
        profile(int num_processes, int process_offset, std::string mode,
            int num_threads, int seed, std::string names, 
            std::string patterns, std::string reads, std::string offsets, 
            std::string ranges, std::string weights, uint64_t _nops, 
            int ngenerators, std::string _name, bool val_flag, bool _unique, 
            double _delay, uint64_t _access_size);
        ~profile();
        void init();  //fill offsets and mmap
        void mmap_run();  //bench mark for mmap
        void run();  //benchmark for non mmap
	void cleanup_files();
        int mmap_validate_io();  //called by run if validate is set
        void summary();  
        
        bool is_mmap()
        {
            return mmap_mode;
        };
        std::vector< std::vector<file_access> > get_offsets()
        {
            return offsets;
        };
        bool is_bad()
        {
            return bad;
        };
    private:
        void offset_summary(uint64_t* file_access); //summarize offsets

        std::vector<weighted_random*> generators;  //hold geneerators
        std::vector< std::vector<file_access> > offsets; //vector of offsets for each thread
        std::vector<int> fd_vec; //file descriptors
        std::vector<std::string> name_list; 
        std::vector<std::string> pattern_list;
        std::vector<uint64_t*> mmap_addr_vec;
        std::vector<uint64_t> mmap_size_vec;
        uint64_t niops ;  //number of io ops for each thread
        uint64_t skipped_ioops; 
        int nthreads; 
        double delay;
        int nprocesses;
        int p_offset;  //determine head process of each profile (MPI_rank == p_offset)
        std::string name;
        bool validate; 
        bool unique;
        bool mmap_mode;
        bool direct_mode;
	bool rmw_mode;
        uint64_t access_size;
        bool bad; 


        //deprecated functions
        void fill_offsets1(); //not used
        int validate_io2(); //not used
};
#endif
