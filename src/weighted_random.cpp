/*
 * Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-624712. 
 * All rights reserved.
 * 
 * This file is part of LRIOT, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Additional BSD Notice. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * • Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the disclaimer below.
 * 
 * • Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the disclaimer (as noted below) in the
 *   documentation and/or other materials provided with the distribution.
 * 
 * • Neither the name of the LLNS/LLNL nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY, LLC,
 * THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * Additional BSD Notice
 * 
 * 1. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at Lawrence Livermore
 * National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * 2. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or
 * process disclosed, or represents that its use would not infringe
 * privately-owned rights.
 * 
 * 3. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring
 * by the United States Government or Lawrence Livermore National Security,
 * LLC. The views and opinions of authors expressed herein do not necessarily
 * state or reflect those of the United States Government or Lawrence Livermore
 * National Security, LLC, and shall not be used for advertising or product
 * endorsement purposes.
 * 
 */
#include "weighted_random.hpp"
#include <limits>
using namespace std;

/*
 *  seq_wrapper is an generator which can generate offsets based on strides
 *  as defined by the user. See the wiki for more information.
*/
seq_wrapper::seq_wrapper( uint64_t _start, uint64_t _stride, uint64_t _end, 
    uint64_t _jump, uint64_t _thread_offset, int _seed, double read, int _tid ):
        generator(_seed), start(_start), stride(_stride), end(_end), 
        jump(_jump), thread_offset(_thread_offset),
        tid(_tid)
{
    //set up READ/WRITE probabiliies
    double prob[2] = {read, 1-read};
    access_generator = boost::random::discrete_distribution<>(prob);

    //initalize current and next start for each thread
    current = start + (tid * thread_offset);
    next_start = start+jump + (tid*thread_offset);

    //set to end, will modify by tid
    next_end = end;
   
    //if next_end modified by tid is smaller than next end
    //set next ent to max
    if( next_end > end + tid*thread_offset)
        next_end = numeric_limits<uint64_t>::max();
    else  //else set normally
        next_end = end+tid*thread_offset;

}

memory_access seq_wrapper::gen_maccess()
{
    //use current
    uint64_t tmp = current; 

    //caclulate next value.
    current += stride;
    //at the end, wil need to jump
    if (current >= next_end)
    {
        current = next_start;
        //prevent wrap around
        if( next_end > (next_end + jump))
            next_end = numeric_limits<uint64_t>::max();
        else
            next_end += jump;
        next_start += jump;
    }

    return std::make_pair<access_type, uint64_t>
         ((access_type)access_generator(generator), tmp);
}

vector<double> seq_wrapper::RW_weights()
{
    return access_generator.probabilities();
}

string seq_wrapper::type()
{
    return "seq";
}

void seq_wrapper::summary()
{
    cout<<", Type: seq, Read Percent: "<<RW_weights()[0]<<", start: "<<start<<
        ", stride: "<<  stride<< 
        ", end: "<<  end << 
        ", jump: "<<  jump  <<
        ", thread_offset: "<<  thread_offset <<endl; 
}

/*
 *  uniform_wrapper is a generator based on boost's random number generator
 *  it generates an interger offset between min and max with equal probability
*/
uniform_wrapper::uniform_wrapper(int seed, double read, uint64_t _offset, 
    uint64_t range ):
        generator(seed), offset(0),
        offset_generator(generator, boost::uniform_int<uint64_t>(0,range))
{
    //initalize weights for R/W
    double prob[2] = {read, 1-read};
    access_generator = boost::random::discrete_distribution<>(prob);
}

memory_access uniform_wrapper::gen_maccess()
{
    return std::make_pair<access_type, uint64_t>
         ((access_type)access_generator(generator), offset+offset_generator());
}

uint64_t uniform_wrapper::min()
{
    return offset_generator.min();
}

uint64_t uniform_wrapper::max()
{
    return offset_generator.max();
}

vector<double> uniform_wrapper::RW_weights()
{
    return access_generator.probabilities();
}

string uniform_wrapper::type()
{
    return "uni";
}

void uniform_wrapper::summary()
{
    cout<<", Type: uni, Read Percent: "<<RW_weights()[0]<<", Offset: "<<offset<<
        " Range: ["<<min()<<","<<max()<<"]"<< endl; 
}

/*
 *  weighted_random is a stucture that holds individual generators.
 *  It is paired with a random generator to select which generator to use.
 *  The probability to select each generator is passed in by the weights.
*/
weighted_random::weighted_random(int seed, int _nfiles, 
    vector<string> pattern_list, vector <double> read_list,
    vector<uint64_t> offset_list, vector<uint64_t> range_list,
    vector<double> _weight_list, uint64_t scale, int tid):
        nfiles(_nfiles), generator(seed)
{
    uint64_t aseed, aoffset, arange;
    double aread, aweight, weight_sum = 0;

    generator_wrapper* tmp;

    //make the generators
    for(int i = 0; i <nfiles; i++)
    {
        aseed = seed+i; //each generator should have a differnt seed
        aread = read_list[i]; 
        aoffset = offset_list[i];  //still mmap the entire file
        //aoffset = 0L; // offset is set with mmap

        //for mmap rage access
        //if first pattern char is *, set to uniform 
        if (pattern_list[0][0] == '*' || pattern_list[i] == "uni")
        {
            //range is divide by scale (for aligned ness)
            // and -1 since its inclusive
            arange = (range_list[i]/scale)-1;
            tmp = (generator_wrapper*) new uniform_wrapper(aseed, aread,
                aoffset,arange);
        }
        //anywhere in the patter is seq
        else if (pattern_list[i].find("seq")>=0)
        {
            //parse the seq pattern
            vector<string> parse_seq;
            boost::split(parse_seq, pattern_list[i], boost::is_any_of("|"), 
                boost::token_compress_on);
            
            uint64_t stride,end, jump, thread_offset;
            stride = boost::lexical_cast<uint64_t> (parse_seq[1]);
            if (parse_seq[2][0] == '*')
            {
                end = range_list[i];
            }else {
	      try{
		end = boost::lexical_cast<uint64_t> (parse_seq[2]);
	      }catch(std::exception &e) {
		std::cout << "Exception caught when converting " << parse_seq[2] << " into an integer value, use full range." << std::endl;
		std::cout << e.what() << std::endl;
		end = range_list[i];
	      }
	    }
            //if there is no jump, set to 0, and use max end
            if (parse_seq[3][0] == '-')
            {
                jump = 0;
                end = numeric_limits<uint64_t>::max();
            }
            else
                jump = boost::lexical_cast<uint64_t> (parse_seq[3]);
            thread_offset = boost::lexical_cast<uint64_t> (parse_seq[4]);
             
            tmp = (generator_wrapper*) new seq_wrapper(aoffset,stride,end,jump,
                thread_offset,aseed,aread,tid);
        }        
        generator_list.push_back(tmp);
        weight_sum += _weight_list[i];
    }

    //set up the weights
    for(int i = 0; i<_weight_list.size();i++)
    {
        weight_list.push_back( _weight_list[i] / weight_sum) ;
    }

    generator_selector = boost::random::discrete_distribution<>(weight_list);
}

weighted_random::~weighted_random()
{
    for (int i = 0; i < generator_list.size();i++)
        delete generator_list[i];
}
file_access weighted_random::gen_faccess()
{
    int file = generator_selector(generator);
    return make_pair<int, memory_access>
        ( file, generator_list[file]->gen_maccess());
}

void weighted_random::summary()
{
    cout<<"Number of distrubtions per generator (should be same as #files): "
        <<generator_list.size()<<endl;
    for(int i =0; i<generator_list.size(); i++)
    {
        cout<<"     Weight: "<<weight_list[i];
        generator_list[i]->summary();
    }
    cout.flush();
}

/*
int main()
{
    seq_wrapper test0 = seq_wrapper(5,2,10,100,100, 0,0,0);
    seq_wrapper test1 = seq_wrapper(5,2,10,100,100, 0,0,1);
    seq_wrapper test2 = seq_wrapper(5,2,10,100,100, 0,0,2);
    seq_wrapper test3 = seq_wrapper(5,2,10,100,100, 0,0,3);
    for(int i = 0; i < 30; i++)
        cout<<test0.get_rand().second<<endl;
    cout<<endl;
    for(int i = 0; i < 30; i++)
        cout<<test1.get_rand().second<<endl;
    cout<<endl;
    for(int i = 0; i < 30; i++)
        cout<<test2.get_rand().second<<endl;
    cout<<endl;
//    for(int i = 0; i < 30; i++)
//        cout<<test3.get_rand().second<<endl;
    cout<<endl;

    test3.summary();
*/
/*    std::vector <generator_wrapper*> list;
    generator_wrapper* tmp = (generator_wrapper*) new uniform_wrapper(7,0,1,.1,1);
    list.push_back(tmp)


    tmp = (generator_wrapper*) new uniform_wrapper(9,0,1,.1, 500);
    list.push_back(tmp);

    int sum = 0;
    memory_access tmp2; 
    for (int i = 0; i < 1000; i++)
    {
        tmp2 = list[0]->get_rand();
        std::cout<<"first: "<< tmp2.first<< " second: "<<tmp2.second<<std::endl;
        sum += tmp2.first;


        tmp2 = list[1]->get_rand();
        std::cout<<"first: "<< tmp2.first<< " second: "<<tmp2.second<<std::endl;
        sum += tmp2.first;


    }
    std::cout<<"sum: "<<sum/2<<std::endl;

    for (int i = 0; i<list.size(); i++)
    {
//        std::cout<<"min" <<i<<": "<<list[i]->min()<<std::endl;
//        std::cout<<"max" <<i<<": "<<list[i]->max()<<std::endl;
        std::cout<< ((uniform_wrapper*)list[i])->RW_weights()[0] <<std::endl;


        delete list[i];
    }
   uint64_t sum;
   uint64_t tmp;
    weighted_random* profile;


    for(int jx = 0; jx<10; jx++)
    {
        profile = new weighted_random(jx ,"./file1:./file2:/file3", "uni:uni:uni","0.1:0.2:0.1", "0:0:0", "200:1024:8192","90:10:50");
    
        cout<<endl;
    
        sum = 0;
        for (int i = 0; i <5000;i++)
        {
            tmp = profile->get_rand().second;
            //cout<<tmp<<endl;
            sum+=tmp;
        }
        cout<<"sum: "<<sum/5000<<endl;
        delete profile;
    }

*/



//}
